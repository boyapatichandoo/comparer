package main

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"golang.org/x/oauth2"
)

var oAuthConfig = &oauth2.Config{}
var oAuthToken map[string]interface{}

func getTokenReponseByURL(tokenURL string) TokenResponse {
	var tokenResponse = TokenResponse{}
	if tokenURL == "" {
		tokenResponse.Token = TokenDetails{}
		tokenResponse.Status = false
		tokenResponse.Error = "Token URL not provided"

		// w.Write(tokenBytes)
		return tokenResponse
	}
	client := &http.Client{}
	response, err := client.Get(tokenURL)
	if err != nil {
		log.Println("Request Error", err)
		tokenResponse.Token = TokenDetails{}
		tokenResponse.Status = false
		tokenResponse.Error = err.Error()
		// w.Write(tokenBytes)
		return tokenResponse
	}
	defer response.Body.Close()
	respBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Panicln("Response Read Error", err)
		tokenResponse.Token = TokenDetails{}
		tokenResponse.Status = false
		tokenResponse.Error = err.Error()
		// w.Write(tokenBytes)
		return tokenResponse
	}
	// fmt.Println("Response Body", string(respBody))
	err = json.Unmarshal(respBody, &tokenResponse.Token)
	if err != nil {
		log.Println("Ummarshal Error", err)
		tokenResponse.Token = TokenDetails{}
		tokenResponse.Status = false
		tokenResponse.Error = err.Error()
		// w.Write(tokenBytes)
		return tokenResponse
	}
	tokenResponse.Status = true
	tokenResponse.Error = ""
	return tokenResponse
}
func getTokenByConfig(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	tokenURL := r.FormValue("tokenURL")
	tokenResponse := getTokenReponseByURL(tokenURL)
	tokenBytes, err := json.Marshal(tokenResponse)
	if err != nil {
		log.Println("Marshal Error", err)

	}
	w.Write(tokenBytes)
}

func getConfig(w http.ResponseWriter, r *http.Request) {
	resBytes, err := json.Marshal(oAuthConfig)
	if err != nil {
		log.Print(err)
		w.Write([]byte(fmt.Sprintf("%s", err)))
		return
	}
	w.Write(resBytes)
}
func getToken(w http.ResponseWriter, r *http.Request) {
	resBytes, err := json.Marshal(oAuthToken)
	if err != nil {
		log.Print(err)
		w.Write([]byte(fmt.Sprintf("%s", err)))
		return
	}
	w.Write(resBytes)
}

// var configMutex = &sync.Mutex{}
// var tokenMutex = &sync.Mutex{}

func addAuthConfig(w http.ResponseWriter, r *http.Request) {
	oauthConfig := []byte(r.FormValue("oauthConfig"))
	err := json.Unmarshal(oauthConfig, &oAuthConfig)
	if err != nil {
		log.Print(err)
		w.Write([]byte("false"))
		return
	}
	// configMutex.Lock()
	// defer configMutex.Unlock()
	// err := createAndWriteToFile("OAuthConfig.json", string(oauthConfig))
	// if err != nil {
	// 	w.Write([]byte(fmt.Sprintf("%s", err)))
	// 	return
	// }
	w.Write([]byte("true"))
}

func getOAuthToken(w http.ResponseWriter, r *http.Request) {

	// var oAuthConfig oauth2.Config
	// err := json.Unmarshal(oauthConfig, &oAuthConfig)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// fmt.Println(oAuthConfig)
	// authurl := oAuthConfig.Endpoint.AuthURL + "?client_id=" + oAuthConfig.ClientID + "&response_type=token&redirect_uri=" + oAuthConfig.RedirectURL + "&scope=" + oAuthConfig.Scopes[0]
	// // authurlX := "https://accounts.google.com/o/oauth2/auth?client_id=412743469409-km068ptnvie1g4ost11ob0rsh2re3kfs.apps.googleusercontent.com&response_type=code&redirect_uri=http://localhost:9000/google/callback&scope=https://www.googleapis.com/auth/userinfo.email"
	// // fmt.Println("url", strings.EqualFold(authurl, authurlX))
	// codeReq, err := http.NewRequest(http.MethodGet, authurl, nil)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// // fmt.Println(authurl)
	// client := http.Client{}
	// response, err := client.Do(codeReq)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// resBytes, err := ioutil.ReadAll(response.Body)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// fmt.Println(string(resBytes))
	// tokenMutex.Lock()
	// defer tokenMutex.Unlock()
	// file, err := os.Open("OAuthToken.json")
	// if err != nil {
	// 	log.Println(err)
	// }
	// tokenBytes, err := ioutil.ReadAll(file)
	// if err != nil {
	// 	log.Println(err)
	// }
	// createAndWriteToFile("OAuthToken.json", "")
	tokenBytes, err := json.Marshal(oAuthToken)
	if err != nil {
		log.Println(err)
	}
	oAuthConfig = &oauth2.Config{}
	oAuthToken = make(map[string]interface{})
	w.Write(tokenBytes)

}

func googleOAuthCallback(w http.ResponseWriter, r *http.Request) {

	// fmt.Printf("%+v", r)
	code := r.FormValue("code")
	// acctoken := r.FormValue("access_token")
	// fmt.Println("access token", acctoken)
	// var oAuthConfig = &oauth2.Config{
	// 	// RedirectURL:  "http://localhost:9000/google/callback",
	// 	// ClientID:     "412743469409-km068ptnvie1g4ost11ob0rsh2re3kfs.apps.googleusercontent.com",
	// 	// ClientSecret: "ZxFMWQbOvWs-AJrhig8Iobnt",
	// 	Scopes:   []string{"https://www.googleapis.com/auth/userinfo.email"},
	// 	Endpoint: oauth2.Endpoint{TokenURL: "https://accounts.google.com/o/oauth2/token"},
	// }
	fmt.Println("Config", oAuthConfig)
	token, err := oAuthConfig.Exchange(context.Background(), code)
	if err != nil {
		fmt.Printf("code exchange error: %s", err.Error())
	}
	tokenBytes, err := json.MarshalIndent(token, "", "  ")
	err = json.Unmarshal(tokenBytes, &oAuthToken)
	fmt.Println("AuthToken", oAuthToken)
	// err = createAndWriteToFile("OAuthToken.json", string(tokenBytes))
	if err != nil {
		log.Println(err)
	}
	t, _ := template.ParseFiles("windowclose.html")
	t.Execute(w, nil)

}
func createAndWriteToFile(file string, data string) error {
	newConfigFile, err := os.Create(file)
	if err != nil {
		log.Print(err)
		return err
	}
	_, err = newConfigFile.WriteString(data)
	if err != nil {
		log.Print(err)
		return err
	}
	return nil
}

// TokenDetails ...
type TokenDetails struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
}

// TokenResponse ...
type TokenResponse struct {
	Token  TokenDetails `json:"token"`
	Status bool         `json:"status"`
	Error  string       `json:"error"`
}
