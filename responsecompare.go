package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

//RequestDetail holds the properties requires to send request
type RequestDetail struct {
	URL     string   `json:"url"`
	Method  string   `json:"method"`
	Headers []Header `json:"headers"`
	Body    string   `json:"body"`
}

// ExcelRequestDetail ...
type ExcelRequestDetail struct {
	Request            RequestDetail
	ResponseTimeCell   string
	IssuesCell         string
	IsResponseSameCell string
	ResponseCell       string
	StatusCell         string
	CanMakeRequest     bool
}

// Header ...
type Header struct {
	Key   string
	Value string
}

var wg sync.WaitGroup

func makeRequestsandCompareData(requestOne, requestTwo RequestDetail) responseStatus {
	wg.Add(2)
	var res1 []byte
	var res2 []byte
	var responseOneLatency, responseTwoLatency string
	var responseOneStatusCode, responseTwoStatusCode string
	var err1, err2, err error
	response := ResponseData{
		requestOneData:    "",
		requestTwoData:    "",
		requestOneLatency: "0",
		requestTwoLatency: "0",
	}
	go func() {
		res1, responseOneLatency, responseOneStatusCode, err1 = getResponse(requestOne)
	}()

	go func() {
		res2, responseTwoLatency, responseTwoStatusCode, err2 = getResponse(requestTwo)
	}()
	wg.Wait()
	response.requestOneLatency = responseOneLatency
	response.responseOneStatusCode = responseOneStatusCode
	response.requestTwoLatency = responseTwoLatency
	response.responseTwoStatusCode = responseTwoStatusCode
	// fmt.Println(string(res1), string(res2))
	if err1 != nil {
		// fmt.Println("Errors:", err1)
		response.requestOneData = err1.Error()
		// return responseStatus{response, false, err1}
	} else {
		if err1 = json.Unmarshal(res1, &response.requestOneData); err != nil {
			fmt.Println("Errors1:", err)
			response.requestOneData = err.Error()
			// return responseStatus{response, false, err}
		}
	}
	if err2 != nil {
		// fmt.Println("Errors:", err2)
		response.requestTwoData = err2.Error()
		// return responseStatus{response, false, err2}
	} else {
		if err2 = json.Unmarshal(res2, &response.requestTwoData); err != nil {
			fmt.Println("Errors2:", err)
			response.requestTwoData = err.Error()
			// return responseStatus{response, false, err}
		}
	}
	// fmt.Println("Same Respone:", string(res1), string(res2))

	// diffCheck(struct{ requestOneData, MuleData interface{} }{requestOneData: response.requestOneData, MuleData: response.MuleData})
	return responseStatus{response, bytes.Equal(res1, res2), err1, err2}
}

func getResponse(request RequestDetail) ([]byte, string, string, error) {
	defer wg.Done()
	client := &http.Client{}
	// fmt.Println(request.Body)
	headerMap := make(http.Header)
	for _, obj := range request.Headers {
		// fmt.Println(obj.Key, ":", obj.Value)
		headerMap.Set(strings.TrimSpace(obj.Key), strings.TrimSpace(obj.Value))
	}
	var requestBody io.Reader = nil
	if strings.ToUpper(request.Method) == "POST" || strings.ToLower(request.Method) == "PUT" {
		bodyMode := headerMap.Get("Content-Type")
		if bodyMode == "application/x-www-form-urlencoded" {
			body := strings.Split(request.Body, "\n")
			var bodyData = url.Values{}
			for _, value := range body {
				item := strings.Split(value, ":")
				if len(item) == 2 {
					bodyData.Set(item[0], item[1])
				}
			}
			requestBody = bytes.NewBuffer([]byte(bodyData.Encode()))
		} else {
			jsonBody, err := json.Marshal(request.Body)
			if err != nil {
				jsonBody = nil
			}
			requestBody = bytes.NewBuffer(jsonBody)
		}
	}
	req, err := http.NewRequest(request.Method, request.URL, requestBody)
	req.Header = headerMap

	response := make([]byte, 0)
	// fmt.Println("request", req)
	requestedAt := time.Now()
	res, err := client.Do(req)
	// fmt.Println("response", res, err)
	latency := time.Since(requestedAt) //.String()
	var responseLatency string
	latencyString := strconv.FormatFloat(float64(latency), 'E', 2, 64)
	roundedUpLatency, parseErr := strconv.ParseFloat(latencyString, 64)
	if parseErr != nil {
		responseLatency = latency.String()
	}
	responseLatency = time.Duration(roundedUpLatency).String()
	if err != nil {
		response = []byte(err.Error())
		return response, responseLatency, fmt.Sprintf("%v", http.StatusNotFound) + " NotFound", err
	}
	defer res.Body.Close()
	contents, err := ioutil.ReadAll(res.Body)
	if err != nil {
		response = []byte(err.Error())
		return response, responseLatency, res.Status, err
	}
	// fmt.Println(string(contents))
	var responseData = make(map[string]interface{})
	if err := json.Unmarshal(contents, &responseData); err != nil {
		fmt.Println(err)
		response = contents
	} else {
		delete(responseData, "correlationId")
		delete(responseData, "messageId")
		response, err = json.Marshal(responseData)
		if err != nil {
			fmt.Println(err)
		}
	}
	// response = contents
	return response, responseLatency, res.Status, nil
}

//ResponseData ...
type ResponseData struct {
	requestOneData        interface{}
	responseOneStatusCode string
	requestTwoData        interface{}
	responseTwoStatusCode string
	requestOneLatency     string
	requestTwoLatency     string
}

type responseStatus struct {
	response       ResponseData
	isSameResponse bool
	errOne         error
	errTwo         error
}
