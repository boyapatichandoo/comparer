module gitlab.com/boyapatichandoo/comparer

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.1.0
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
