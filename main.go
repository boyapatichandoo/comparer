package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"html/template"
	"image/gif"
	"io"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
)

func main() {

	http.HandleFunc("/", homePage)
	http.HandleFunc("/compare", compare)
	http.HandleFunc("/addAuthConfig", addAuthConfig)
	http.HandleFunc("/google/callback", googleOAuthCallback)
	http.HandleFunc("/getToken", getOAuthToken)
	http.HandleFunc("/getConfig", getConfig)
	http.HandleFunc("/uploadFile", upload)
	http.HandleFunc("/getTokenByConfig", getTokenByConfig)
	http.HandleFunc("/loader", getLoaderGif)
	http.ListenAndServe(":9000", nil)

}

var filename string

func createFile(r *http.Request) error {
	file, handler, err := r.FormFile("file")
	if err != nil {
		log.Println(err)
		return err
	}
	defer file.Close()
	f, err := os.Create(handler.Filename)
	if err != nil {
		log.Println(err)
		return err
	}
	defer f.Close()
	filename = handler.Filename
	fmt.Println(filename)
	_, err = io.Copy(f, file)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil

}

func upload(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Start")
	// r.ParseMultipartForm(32 << 20)
	err := createFile(r)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		// fmt.Println(err.Error())
		return
	}
	excel, err := excelize.OpenFile(filename)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	rows, err := excel.Rows("Sheet1")
	if err != nil {
		log.Fatal(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("An error occured while opening file. Please try again"))
		return
	}
	var requestsListMap = make(map[string][]ExcelRequestDetail)
	var tokenResponse TokenResponse
	var bulkResponseMap = make(map[string]BulkResponse)
	var once sync.Once
	var row = 1
	for rows.Next() {
		rowCells, _ := rows.Columns()
		if row == 1 {
			row++
			continue
		}
		var excelRequest = ExcelRequestDetail{Request: RequestDetail{}}
		excelRequest.IsResponseSameCell = "H" + fmt.Sprintf("%v", row)
		clearOutputCellsInTheSheet(excel, "Sheet1", excelRequest.IsResponseSameCell)
		excelRequest.ResponseTimeCell = "I" + fmt.Sprintf("%v", row)
		clearOutputCellsInTheSheet(excel, "Sheet1", excelRequest.ResponseTimeCell)
		excelRequest.IssuesCell = "J" + fmt.Sprintf("%v", row)
		clearOutputCellsInTheSheet(excel, "Sheet1", excelRequest.IssuesCell)
		excelRequest.ResponseCell = "K" + fmt.Sprintf("%v", row)
		clearOutputCellsInTheSheet(excel, "Sheet1", excelRequest.ResponseCell)
		excelRequest.StatusCell = "L" + fmt.Sprintf("%v", row)
		clearOutputCellsInTheSheet(excel, "Sheet1", excelRequest.StatusCell)
		excelRequest.Request.Method = rowCells[2]
		excelRequest.Request.URL = rowCells[3]
		excelRequest.Request.Body = rowCells[5]
		headers := strings.Split(rowCells[4], "\n")
		excelRequest.Request.Headers = make([]Header, 0)
		if strings.ToLower(strings.TrimSpace(rowCells[6])) == "yes" {
			once.Do(func() {
				var tokenURL string
				sheetTwoRows, err := excel.Rows("Sheet2")
				if err != nil {
					log.Fatal(err)
					if err := excel.SetCellValue("Sheet1", excelRequest.IssuesCell, "Unable to fetch token details"); err != nil {
						log.Println("ExcelWrite", err)
					}
					return
				}
				var rowNumber = 1
				for sheetTwoRows.Next() {
					sheetTwoRow, err := sheetTwoRows.Columns()
					if err != nil {
						log.Fatal(err)
						if err := excel.SetCellValue("Sheet1", excelRequest.IssuesCell, "Unable to fetch token details"); err != nil {
							log.Println("ExcelWrite", err)
						}
						return
					}
					if rowNumber == 1 {
						// rowNumber++
						// continue
						tokenURL = sheetTwoRow[1]
						// fmt.Println("tokenURL", tokenURL)
						break
					} // else if rowNumber == 2 {
					// 	tokenURL = sheetTwoRow[1]
					// 	fmt.Println("tokenURL", tokenURL)
					// 	break
					// }
				}
				tokenResponse = getTokenReponseByURL(tokenURL)
			})
			if tokenResponse.Status == false {
				if err := excel.SetCellValue("Sheet1", excelRequest.IssuesCell, "Unable to fetch token details"); err != nil {
					log.Println("ExcelWrite", err)
				}
				if style, err := excel.NewStyle(`{"font":{"color":"#F28E8E"}}`); err != nil {
					log.Println("Excel Cell Color", err)
				} else {
					err = excel.SetCellStyle("Sheet1", excelRequest.IssuesCell, excelRequest.IssuesCell, style)
				}
				excelRequest.CanMakeRequest = true

			} else {
				authHeader := Header{Key: "Authorization", Value: "Bearer " + tokenResponse.Token.AccessToken}
				excelRequest.Request.Headers = append(excelRequest.Request.Headers, authHeader)
				excelRequest.CanMakeRequest = true

			}

		} else {
			excelRequest.CanMakeRequest = true
		}
		for _, value := range headers {
			hdr := strings.Split(value, ":")
			if len(hdr) == 2 {
				header := Header{Key: strings.TrimSpace(hdr[0]), Value: hdr[1]}
				excelRequest.Request.Headers = append(excelRequest.Request.Headers, header)
			}
		}
		row++
		if _, ok := requestsListMap[rowCells[1]]; !ok {
			requestsListMap[rowCells[1]] = make([]ExcelRequestDetail, 1, 2)
			requestsListMap[rowCells[1]][0] = excelRequest
		} else if len(requestsListMap[rowCells[1]]) != 2 {
			requestsListMap[rowCells[1]] = append(requestsListMap[rowCells[1]], excelRequest)
		}
	}

	var comparisionResponseMap = make(map[string]chan responseStatus)
	for endpoint, requestList := range requestsListMap {
		if len(requestList) == 2 && requestList[0].CanMakeRequest && requestList[1].CanMakeRequest {
			comparisionResponseMap[endpoint] = make(chan responseStatus, 1)
		} else if len(requestList) == 2 && (!requestList[0].CanMakeRequest && !requestList[1].CanMakeRequest) {
			bulkResponseMap[endpoint] = BulkResponse{EndPoint: endpoint, RequestOne: requestList[0].Request, RequestTwo: requestList[1].Request, Success: false, Issues: []string{"Unable to fetch token", "Unable to fetch token"}}

		} else if len(requestList) == 2 && (requestList[0].CanMakeRequest && !requestList[1].CanMakeRequest) {
			bulkResponseMap[endpoint] = BulkResponse{EndPoint: endpoint, RequestOne: requestList[0].Request, RequestTwo: requestList[1].Request, Success: false, Issues: []string{"Did not made request", "Unable to fetch token"}}

		} else if len(requestList) == 2 && (!requestList[0].CanMakeRequest && requestList[1].CanMakeRequest) {
			bulkResponseMap[endpoint] = BulkResponse{EndPoint: endpoint, RequestOne: requestList[0].Request, RequestTwo: requestList[1].Request, Success: false, Issues: []string{"Unable to fetch token", "Did not made request"}}

		} else if len(requestList) == 1 && (!requestList[0].CanMakeRequest) {
			bulkResponseMap[endpoint] = BulkResponse{EndPoint: endpoint, RequestOne: requestList[0].Request, Success: false, Issues: []string{"Unable to fetch token"}}
		}
	}

	var compareMutex sync.Mutex
	for endpoint, requestList := range requestsListMap {
		if len(requestList) == 2 && requestList[0].CanMakeRequest && requestList[1].CanMakeRequest {
			go func(endpoint string, requestList []ExcelRequestDetail) {
				response := makeRequestsandCompareData(requestList[0].Request, requestList[1].Request)
				compareMutex.Lock()
				defer compareMutex.Unlock()
				comparisionResponseMap[endpoint] <- response
			}(endpoint, requestList)
		} else if len(requestList) == 1 {
			excel.SetCellValue("Sheet1", requestList[0].IssuesCell, "Other request is missing to compare")
			bulkResponseMap[endpoint] = BulkResponse{EndPoint: endpoint, RequestOne: requestList[0].Request, Success: false, Issues: []string{"Other request is missing to compare"}}

		}
	}

	var excelMutex = &sync.Mutex{}
	var responseMutex = &sync.Mutex{}
	var wg = &sync.WaitGroup{}
	for endpoint, comparisionResponse := range comparisionResponseMap {
		wg.Add(1)
		go func(endpoint string, comparisionResponse chan responseStatus) {
			defer wg.Done()
			response := <-comparisionResponse
			comparedResponse := checkAndGetComparedOutput(response)
			requestList := requestsListMap[endpoint]
			responseMutex.Lock()
			defer responseMutex.Unlock()
			bulkResponseMap[endpoint] = BulkResponse{EndPoint: endpoint, RequestOne: requestList[0].Request, RequestTwo: requestList[1].Request, Success: true, Response: comparedResponse, Issues: []string{}}

			writeResponseInExcelSheet(requestList, response, excel, "Sheet1", excelMutex)
		}(endpoint, comparisionResponse)
	}
	wg.Wait()
	if err := excel.Save(); err != nil {
		log.Println(err)
	}
	var bulkResponse = make([]BulkResponse, 0)
	for _, response := range bulkResponseMap {
		bulkResponse = append(bulkResponse, response)
	}
	sort.Slice(bulkResponse, func(i int, j int) bool {
		endpointOne, err := strconv.Atoi(bulkResponse[i].EndPoint)
		if err != nil {
			log.Println("Sort Error", err.Error())
		}
		endpointTwo, err := strconv.Atoi(bulkResponse[j].EndPoint)
		if err != nil {
			log.Println("Sort Error", err.Error())
		}
		return endpointOne < endpointTwo

	})
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(bulkResponse); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(""))
	}
	// fmt.Println(bulkResponse)
	fmt.Println("End")
}

func writeResponseInExcelSheet(comparedRequests []ExcelRequestDetail, response responseStatus, excelSheet *excelize.File, sheetName string, mutex *sync.Mutex) {
	mutex.Lock()
	defer mutex.Unlock()
	if err := excelSheet.SetCellValue(sheetName, comparedRequests[0].StatusCell, response.response.responseOneStatusCode); err != nil {
		log.Println("ExcelWrite", err)
	}
	if err := excelSheet.SetCellValue(sheetName, comparedRequests[1].StatusCell, response.response.responseTwoStatusCode); err != nil {
		log.Println("ExcelWrite", err)
	}
	if err := excelSheet.SetCellValue(sheetName, comparedRequests[0].ResponseTimeCell, response.response.requestOneLatency); err != nil {
		log.Println("ExcelWrite", err)
	}
	if err := excelSheet.SetCellValue(sheetName, comparedRequests[1].ResponseTimeCell, response.response.requestTwoLatency); err != nil {
		log.Println("ExcelWrite", err)
	}
	var responseOneData, responseTwoData string
	jsonBytes, err := json.Marshal(response.response.requestOneData)
	if err != nil {
		log.Println(err)
		if err := excelSheet.SetCellValue(sheetName, comparedRequests[0].IssuesCell, "error occured while while setting response"); err != nil {
			log.Println("ExcelWrite", err)
		}
		responseOneData = ""
	} else {
		responseOneData = string(jsonBytes)
	}
	jsonBytes = []byte("")
	if err := excelSheet.SetCellValue(sheetName, comparedRequests[0].ResponseCell, responseOneData); err != nil {
		log.Println("ExcelWrite", err)
	}
	jsonBytes, err = json.Marshal(response.response.requestTwoData)
	if err != nil {
		log.Println(err)
		if err := excelSheet.SetCellValue(sheetName, comparedRequests[1].IssuesCell, "error occured while while setting response"); err != nil {
			log.Println("ExcelWrite", err)
		}
		responseTwoData = ""
	} else {
		responseTwoData = string(jsonBytes)
	}
	if err := excelSheet.SetCellValue(sheetName, comparedRequests[1].ResponseCell, responseTwoData); err != nil {
		log.Println("ExcelWrite", err)
	}
	if response.errOne != nil || response.errTwo != nil {
		if style, err := excelSheet.NewStyle(`{"font":{"color":"#F28E8E"}}`); err != nil {
			log.Println("Excel Cell Color", err)
		} else {
			err = excelSheet.SetCellStyle("Sheet1", comparedRequests[0].IssuesCell, comparedRequests[0].IssuesCell, style)
			err = excelSheet.SetCellStyle("Sheet1", comparedRequests[1].IssuesCell, comparedRequests[1].IssuesCell, style)
		}
		if err := excelSheet.SetCellValue(sheetName, comparedRequests[0].IssuesCell, response.errOne); err != nil {
			log.Println("ExcelWrite", err)
		}
		if err := excelSheet.SetCellValue(sheetName, comparedRequests[1].IssuesCell, response.errTwo); err != nil {
			log.Println("ExcelWrite", err)
		}
		return
	}
	// fmt.Println("excel", response.isSameResponse)

	var isSameResponseCellColor string = "#F28E8E"
	if response.isSameResponse {
		isSameResponseCellColor = "#A6EFC6"
	}
	if strings.Contains(response.response.responseOneStatusCode, "200") && strings.Contains(response.response.responseTwoStatusCode, "200") {
		if err := excelSheet.MergeCell(sheetName, comparedRequests[0].IsResponseSameCell, comparedRequests[1].IsResponseSameCell); err != nil {
			log.Println("ExcelCellMerge", err)
			if err := excelSheet.SetCellValue(sheetName, comparedRequests[0].IsResponseSameCell, strconv.FormatBool(response.isSameResponse)); err != nil {
				log.Println("ExcelWrite", err)
			}

			if style, err := excelSheet.NewStyle(`{"fill":{"type":"pattern","color":["` + isSameResponseCellColor + `"],"pattern":1},"font":{"bold":true},"alignment":{"wrap_text":true}}`); err != nil {
				log.Println("Excel Cell Color", err)
			} else {
				err = excelSheet.SetCellStyle("Sheet1", comparedRequests[0].IsResponseSameCell, comparedRequests[0].IsResponseSameCell, style)
			}
			if err := excelSheet.SetCellValue(sheetName, comparedRequests[1].IsResponseSameCell, strconv.FormatBool(response.isSameResponse)); err != nil {
				log.Println("ExcelWrite", err)
			}
			if style, err := excelSheet.NewStyle(`{"fill":{"type":"pattern","color":["` + isSameResponseCellColor + `"],"pattern":1},"font":{"bold":true},"alignment":{"wrap_text":true}}`); err != nil {
				log.Println("Excel Cell Color", err)
			} else {
				err = excelSheet.SetCellStyle("Sheet1", comparedRequests[1].IsResponseSameCell, comparedRequests[1].IsResponseSameCell, style)
			}
			return
		}
		// fmt.Println("excel", response.isSameResponse, comparedRequests[0].IsResponseSameCell, comparedRequests[1].IsResponseSameCell)
		if err := excelSheet.SetCellValue(sheetName, comparedRequests[0].IsResponseSameCell, strconv.FormatBool(response.isSameResponse)); err != nil {
			log.Println("ExcelWrite", err)
		}
		if style, err := excelSheet.NewStyle(`{"fill":{"type":"pattern","color":["` + isSameResponseCellColor + `"],"pattern":1},"font":{"bold":true},"alignment":{"wrap_text":true}}`); err != nil {
			log.Println("Excel Cell Color", err)
		} else {
			err = excelSheet.SetCellStyle("Sheet1", comparedRequests[0].IsResponseSameCell, comparedRequests[1].IsResponseSameCell, style)
		}
		return
	}

	// fmt.Println(excelSheet.GetCellValue(sheetName, "H2"))
	// fmt.Println(excelSheet.GetCellValue(sheetName, "H3"))
	return

}

func homePage(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("compare.html")
	t.Execute(w, nil)
}

func compare(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// fmt.Println("received")
	// r.ParseForm()

	var requestOne, requestTwo []byte = make([]byte, 0), make([]byte, 0)
	// for _, val := range r.FormValue("requestOne") {
	requestOne = []byte(r.FormValue("requestOne"))
	// }
	// for _, val := range r.FormValue("requestTwo") {
	requestTwo = []byte(r.FormValue("requestTwo"))
	// }

	// fmt.Println(requestOne, requestTwo)
	var reqOne, reqTwo RequestDetail
	err1, err2 := json.Unmarshal(requestOne, &reqOne), json.Unmarshal(requestTwo, &reqTwo)
	if err1 != nil {
		fmt.Println("err1", err1)
		return
	}
	if err2 != nil {
		fmt.Println("err2", err2)
		return
	}

	response := makeRequestsandCompareData(reqOne, reqTwo)
	requestResponse := checkAndGetComparedOutput(response)
	jsonRes, err := json.Marshal(requestResponse)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(""))
	}
	fmt.Printf("reqOne %+v", string(jsonRes))
	w.WriteHeader(http.StatusOK)
	w.Write(jsonRes)
	return
}
func clearOutputCellsInTheSheet(excelSheet *excelize.File, sheet string, cell string) {
	if err := excelSheet.SetCellValue(sheet, cell, ""); err != nil {
		log.Println("ExcelWrite", err)
	}
}
func checkAndGetComparedOutput(response responseStatus) Response {
	var res Response
	// defer fmt.Println("res", res, response)
	if response.errOne != nil || response.errTwo != nil {
		log.Println(response.errOne, response.errTwo)
		res.Data = "Error occured in making request.Check provide urls and data."
		res.Status = "Failed"
		res.RequestOneData = response.response.requestOneData
		res.RequestTwoData = response.response.requestTwoData
		res.ResponseOneLatency = response.response.requestOneLatency
		res.ResponseTwoLatency = response.response.requestTwoLatency
		res.Success = false
		// jsonRes, err := json.Marshal(res)
		// if err != nil {
		// 	// w.WriteHeader(http.StatusInternalServerError)
		// 	// w.Write([]byte(""))
		// 	return http.StatusInternalServerError, []byte("")
		// }
		// w.WriteHeader(http.StatusOK)
		// w.Write(jsonRes)
		return res
	}
	res.Status = "Success"
	res.RequestOneData = response.response.requestOneData
	res.RequestTwoData = response.response.requestTwoData
	res.ResponseOneLatency = response.response.requestOneLatency
	res.ResponseTwoLatency = response.response.requestTwoLatency
	if strings.Contains(response.response.responseOneStatusCode, "200") && strings.Contains(response.response.responseTwoStatusCode, "200") {
		res.Success = true
		if response.isSameResponse {
			res.Data = "Same"
			res.SameResponse = true
		} else {
			res.Data = "Different"
			res.SameResponse = false
		}
		return res
	}
	res.Data = "Exception"
	res.Success = false
	// fmt.Println(res)
	// jsonRes, err := json.Marshal(res)
	// if err != nil {
	// 	// w.WriteHeader(http.StatusInternalServerError)
	// 	// w.Write([]byte(""))
	// 	return http.StatusInternalServerError, []byte("")
	// }
	// fmt.Println(string(jsonRes))
	// w.WriteHeader(http.StatusOK)
	// w.Write(jsonRes)
	return res
}

// Response ...
type Response struct {
	SameResponse       bool        `json:"sameResponse"`
	Success            bool        `json:"success"`
	Status             string      `json:"status"`
	Data               interface{} `json:"data"`
	RequestOneData     interface{} `json:"requestOneData"`
	RequestTwoData     interface{} `json:"requestTwoData"`
	ResponseOneLatency interface{} `json:"responseOneLatency"`
	ResponseTwoLatency interface{} `json:"responseTwoLatency"`
}

// BulkResponse ...
type BulkResponse struct {
	EndPoint   string        `json:"endpoint"`
	RequestOne RequestDetail `json:"requestOne"`
	RequestTwo RequestDetail `json:"requestTwo"`
	Response   Response      `json:"response"`
	Success    bool          `json:"success"`
	Issues     []string      `json:"issues"`
}

func getLoaderGif(w http.ResponseWriter, r *http.Request) {
	inputFile, err := os.Open("Ripple.gif")
	defer inputFile.Close()
	if err != nil {
		log.Println(err)
	}

	bufioReader := bufio.NewReader(inputFile)

	gifFile, err := gif.DecodeAll(bufioReader)
	if err != nil {
		log.Println(err)
	}
	w.Header().Set("Content-Type", "image/gif")
	err = gif.EncodeAll(w, gifFile)
	if err != nil {
		log.Println(err)
		w.Header().Set("Content-Type", "text")
		w.Write([]byte("http://i.stack.imgur.com/MnyxU.gif"))
	}
}
